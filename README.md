# Form for Dish

React implementation of Form for create your Dish 

## Demo

Try this app [https://dominik-barchanski.netlify.app]

### Getting started 

$ npm install <br />
$ git clone https://DominikBarchanski@bitbucket.org/DominikBarchanski/form.git<br />
$ cd form<br />
$ npm start<br />
#open http://localhost:3000<br /><br />

You can also acces the local development server for mobile<br /><br />

$ npm install <br />
$ git clone https://DominikBarchanski@bitbucket.org/DominikBarchanski/form.git<br />
$ cd form<br />
$ npm start<br />
#open http://localhost:3000<br />

#### License

MIT
