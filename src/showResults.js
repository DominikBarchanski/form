export default (async function ShowResults(values) {

    let bodyOfJSON = JSON.stringify(values);


    fetch('https://frosty-wood-6558.getsandbox.com:443/dishes', {
        headers: {
            "Content-Type": "application/json"
        },
        method: "POST",
        body: bodyOfJSON
    }).then(res => res.json())
        .then(response => console.log('Success:', JSON.stringify(response)))
        .catch(error => console.error('Error:', error));

});
