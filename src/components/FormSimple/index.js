import React from 'react';
import {Field, formValueSelector, reduxForm} from 'redux-form';
import {connect} from 'react-redux';

import 'antd/dist/antd.css';

const validate = values => {
    const errors = {}
    if (!values.name) {
        errors.name = "Required";
    }
    if (!values.preparation_time) {
        errors.preparation_time = "Required"
    }

    if (values.type === 'pizza') {
        if (!values.no_of_slices) errors.no_of_slices = "Required"
        if (!values.diameter) errors.diameter = "Required"
    }
    if (values.type === 'soup') {
        if (!values.spiciness_scale) errors.spiciness_scale = "Required"
    }
    if (values.type === 'sandwich') {
        if (!values.slices_of_bread) errors.slices_of_bread = "Required"
    }
    return errors;
}

const renderField = ({
                         input,
                         step = null,
                         value = undefined,
                         min = undefined,
                         max = undefined,
                         parse = undefined,
                         label,
                         type,
                         meta: {touched, error}
                     }) => (
    <div>
        <label>{label}</label>
        <div>
            <input {...input} placeholder={label} type={type} step={step} value={value} min={min} max={max}
                   parse={parse}/>
            {touched && ((error && <div className={"error"}>{error}</div>))}
        </div>
    </div>
)

let FormSimple = props => {


    const {handleSubmit, selectedDish, pristine, reset, submitting} = props;

    return (
        <form onSubmit={handleSubmit}>

            <div>
                <Field name="name" type="text" component={renderField} label="Dish name"/>

            </div>

            <div>
                <Field name="preparation_time" type="time" step="1" component={renderField} label="Preparation Time"/>
            </div>
            <div>
                <label>Type of your dish</label>
                <div>

                    <label><Field name="type" component="input" type="radio" value="pizza"/> Pizza </label><br/>
                    <label><Field name="type" component="input" type="radio" value="soup"/> Soup </label><br/>
                    <label><Field name="type" component="input" type="radio"
                                  value="sandwich"/> Sandwich</label><br/>
                </div>
                {selectedDish === 'pizza' ? (
                    <div>
                        <Field name="no_of_slices" type="number" min="1" component={renderField}
                               label="Number of Slice" parse={(val => parseInt(val, 10))}/>
                        <Field name="diameter" type="number" min="1" step='0.1' component={renderField}
                               label="Diameter" parse={(val => parseFloat(val))}/>
                    </div>

                ) : selectedDish === 'soup' ? (
                    <div>

                        <Field name="spiciness_scale" type="number" min='1' max='10' component={renderField}
                               label="Spiciness Scale" parse={(val => parseInt(val, 10))}/>

                    </div>
                ) : selectedDish === 'sandwich' ? (
                    <div>
                        <div>
                            <Field name="slices_of_bread" type="number" min="1" component={renderField}
                                   label="Slices Of Bread" parse={(val => parseInt(val, 10))}/>
                        </div>
                    </div>
                ) : null

                }
            </div>
            <div>
                <div className="button">
                    <button type='submit' disabled={pristine || submitting}>Submit</button>
                    <button type='button' disabled={pristine || submitting} onClick={reset}>Clear values</button>
                </div>
            </div>


        </form>


    )


};

FormSimple = reduxForm({
    form: 'formDish',
    validate
})(FormSimple);


const selector = formValueSelector("formDish");

FormSimple = connect(state => {

    const name = selector(state, 'name')
    const selectedDish = selector(state, 'type');

    return {
        name,
        selectedDish

    };
})(FormSimple);
export default FormSimple