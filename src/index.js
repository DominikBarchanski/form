import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import store from './store';
import FormSimple from './components/FormSimple';
import showResults from "./showResults";
import './index.css';
const rootElement = document.getElementById('root');

ReactDOM.render(
  <Provider store={store}>
    <div className={"formSimple"}>
        <h2>Dish</h2>
        <FormSimple onSubmit={showResults}/>

    </div>

  </Provider> ,
    rootElement



);


